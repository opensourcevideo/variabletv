Variable Television
================

Using the Pirate PI image available via http://piratebox.aod-rpg.de/dokuwiki/doku.php/raspberry

I start by flashing the Pirate PI image to an SD card:

 sudo dd bs=4M if=2013-02-09-wheezy-raspbian.img of=/dev/sdb

In this case it's a 4GB class 4 SD that I've known to work well (if slow) with the PI.

Following the Pirate PI instructions:

* Enable SSH Server via raspberry-config
* change Password
* Expand FS
* Finish & reboot now

Refreshed the repos:

 apt-get update

then:

 sudo apt-get install mplayer ffmpeg

This takes a while ... ffmpeg is used to get the duration of a video to pick a suitable random position, and mplayer is used to actually do the media playback.

One trouble I had was that lighttpd runs as user "nobody" with "nogroup" which leads to permissions problems as the pi user couldn't then access uploaded movies.

sudo nano /opt/pireatebox/conf/piratebox.conf, and changed lines at end to:

 LIGHTTPD_USER=www-data
 LIGHTTPD_GROUP=www-data


Then, set up the PI to auto-login as the PI user, following the instructions here:
http://www.opentechguides.com/how-to/article/raspberry-pi/5/raspberry-pi-auto-start.html


Auto-login (to shell)
------------------

Step 1: Open a terminal session and edit inittab file.
 sudo nano /etc/inittab

Step 2: Disable the getty program. Navigate to the following line in inittab

 1:2345:respawn:/sbin/getty 115200 tty1

And add a # at the beginning of the line to comment it out

 #1:2345:respawn:/sbin/getty 115200 tty1

Step 3: Add login program to inittab.

 Add the following line just below the commented line

 1:2345:respawn:/bin/login -f pi tty1 </dev/tty1 >/dev/tty1 2>&1

This will run the login program with pi user and without any authentication

Step 4: Save and Exit.

Press Ctrl+X to exit nano editor followed by Y to save the file.


Install the variabletv player script
-------------------------------

then:
 cd $HOME
 sudo apt-get install git
 git clone git://gitorious.org/opensourcevideo/variabletv.git



Auto-run the script at login
--------------------------

If you need to automatically run a script at every login you can do so by adding the script on to the file /etc/profile

Step 1: Open a terminal session and edit the file /etc/profile

 sudo nano /etc/profile

Step 2: Add the following line to the end of the file
 . /home/pi/variabletv/play.py

replace the script name and path with correct name and path of your start-up script.

Step 3: Save and Exit

 Press Ctrl+X to exit nano editor followed by Y to save the file.


Setup piratebox to auto start
-------------------------

 sudo update-rc.d piratebox  defaults

