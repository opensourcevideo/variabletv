#!/usr/bin/env python
#-*- coding:utf-8 -*-

from subprocess import Popen, PIPE, STDOUT
import sys, re, glob, os
from random import choice, random
from time import sleep


FFMPEG = "ffmpeg"
MPLAYER = "mplayer"
VIDEOS = "/opt/piratebox/share/Shared/"
dpat = re.compile(r"Duration:\s*(?P<h>\d\d):(?P<m>\d\d):(?P<s>\d\d(\.\d+)?)", re.I)
EXT = "mp4 webm ogv ogg avi mov qt mpeg m2t"
FRAGMENTDURATION = 10.0

extensions = {}
for ext in EXT.split():
    extensions[ext] = True

def random_file ():
    ret = []
    for f in os.listdir(VIDEOS):
        if f.startswith("."):
            continue
        (base, ext) = os.path.splitext(f)
        ext = ext.lower().lstrip(".")
        if ext in extensions:
            ret.append(os.path.join(VIDEOS, f))
    return choice(ret)

def ffmpeg_get_duration (path):
    cmd = '{0} -i "{1}"'.format(FFMPEG, path)
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    data = p.stdout.read()

    # print "got"
    # print data
    m = dpat.search(data)
    if m:
        d = m.groupdict()
        ss = int(d.get("h"))* 3600
        ss += int(d.get("m"))*60
        ss += float(d.get("s"))
        return ss

def play_random_fragment (p):
    totalduration = ffmpeg_get_duration(f)
    playduration = min(totalduration, FRAGMENTDURATION)
    start = random() * (totalduration-playduration)
    cmd = '{0} -fs -ss {1} -endpos {2} "{3}"'.format(MPLAYER, start, playduration, p)
    print cmd
    os.system(cmd)

# path = sys.argv[1]
while True:
    f = random_file()
    play_random_fragment(f)
    sleep(1)


